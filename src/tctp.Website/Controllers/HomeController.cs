﻿using System.Linq;
using System.Web.Mvc;
using tctp.BusinessFacade.Parser;
using tctp.DataService;
using tctp.Domain;
using tctp.Website.Models;

namespace tctp.Website.Controllers
{
    public class HomeController : Controller
    {
        private readonly IParserManager<Transaction> parserManager;
        private readonly ITransactionRepository transactionRepository;
        private readonly IRepository<Error> errorRepository;


        public HomeController()
        {
            this.parserManager = new ParserManager();
            this.transactionRepository = new TransactionRepository();
            this.errorRepository = new ErrorRepository();
        }

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(FileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            var data = parserManager.Parse(model.File.InputStream,
                model.File.FileName,
                out var errors);

            if (errors.Any())
            {
                errorRepository.Add(errors);
                return RedirectToAction("Failed");
            }

            if (data.Any())
            {
                transactionRepository.Add(data);
            }

            return RedirectToAction("Success");
        }


        [HttpGet]
        public ActionResult Failed()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Success()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Api()
        {
            return View();
        }
    }
}