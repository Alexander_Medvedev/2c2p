﻿using System.Web.Http;
using tctp.DataService;
using tctp.Domain;

namespace tctp.Website.Controllers
{
    public class ErrorApiController : ApiController
    {
        private readonly IRepository<Error> repositorey;

        public ErrorApiController()
        {
            repositorey = new ErrorRepository();
        }

        [HttpGet]
        [Route("api/errors", Name = "GetAllError")]
        public IHttpActionResult Get()
        {
            return Ok(repositorey.GetAll());
        }
    }
}