﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using tctp.BusinessFacade.Model;
using tctp.DataService;
using tctp.Domain;
using tctp.Domain.Defenition;
using tctp.Website.Models;

namespace tctp.Website.Controllers
{
    public class TransactionApiController : ApiController
    {
        private readonly ITransactionRepository repositorey;

        public TransactionApiController()
        {
            repositorey = new TransactionRepository();
        }

        [HttpGet]
        [Route("api/transactions", Name = "GetAll")]
        public IHttpActionResult Get()
        {
            return Ok(Map(repositorey.GetAll()));
        }

        [HttpGet]
        [Route("api/transactions/currency/{code}", Name = "GetByCurrency")]
        public IHttpActionResult GetByCurrency(string code)
        {
            if (string.IsNullOrEmpty(code))
                return BadRequest("Invalid currency");

            return Ok(Map(repositorey.GetByCurrency(code)));
        }

        [HttpGet]
        [Route("api/transactions/date/{from}/{to}", Name = "GetByDate")]
        public IHttpActionResult GetByDate(DateTime? from, DateTime? to)
        {
            if (!from.HasValue || !to.HasValue)
                return BadRequest("Invalid date range");

            return Ok(Map(repositorey.GetByDateRange(from.Value, to.Value)));
        }

        [HttpGet]
        [Route("api/transactions/status/{status}", Name = "GetByStatus")]
        public IHttpActionResult GetByStatus(string status)
        {
            if (!Enum.TryParse(status, true, out Status theStatus))
                return BadRequest("Invalid status");

            return Ok(Map(repositorey.GetByStatus(theStatus)));
        }

        private static List<TransactionViewModel> Map(IEnumerable<Transaction> transactions)
        {
            return transactions.Select(x => new TransactionViewModel
            {
                Id = x.TransactionId,
                Payment = $"{x.Amount:.00} {x.CurrencyCode}",
                Status = x.Status.ToString()
            }).ToList();
        }
    }
}