﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using tctp.Website.Attributes;

namespace tctp.Website.Models
{
    public class FileViewModel
    {
        [Required]
        [MaxFileSize(1024 * 1024, ErrorMessage = "Maximum allowed file size is {0}.")]
        [AcceptedFileExtension("csv|xml", ErrorMessage = "Only the following file types are allowed: {0}.")]
        public HttpPostedFileBase File { get; set; }
    }
}