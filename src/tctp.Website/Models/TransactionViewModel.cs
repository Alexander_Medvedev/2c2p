﻿namespace tctp.Website.Models
{
    public class TransactionViewModel
    {
        public string Id { get; set; }
        public string Payment { get; set; }
        public string Status { get; set; }
    }
}