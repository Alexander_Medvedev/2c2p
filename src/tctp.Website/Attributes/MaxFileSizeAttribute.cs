﻿using System.ComponentModel.DataAnnotations;
using System.Web;
using tctp.SystemFacade.Defenition;
using tctp.SystemFacade.Extensions;

namespace tctp.Website.Attributes
{
    public class MaxFileSizeAttribute : ValidationAttribute
    {
        private readonly long maxFileSize;

        public MaxFileSizeAttribute(long maxFileSize)
        {
            this.maxFileSize = maxFileSize;
        }

        public override bool IsValid(object value)
        {
            if (!(value is HttpPostedFileBase file))
            {
                return false;
            }

            return file.ContentLength <= this.maxFileSize;
        }

        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(this.maxFileSize.ToSize(SizeUnits.MB));
        }
    }
}