﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace tctp.Website.Attributes
{
    public class AcceptedFileExtensionAttribute : ValidationAttribute
    {
        private readonly string[] validTypes;

        public AcceptedFileExtensionAttribute(string validTypes)
        {
            this.validTypes = validTypes
                .Split(new[] {'|'}, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Trim().ToLower())
                .ToArray();
        }

        public override bool IsValid(object value)
        {
            if (!(value is HttpPostedFileBase file))
            {
                return false;
            }

            return validTypes.Any(x => file.FileName.ToLower().EndsWith(x));
        }

        public override string FormatErrorMessage(string name)
        {
            return base.FormatErrorMessage(string.Join(", ", this.validTypes));
        }
    }
}