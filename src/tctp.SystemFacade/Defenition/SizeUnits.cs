﻿namespace tctp.SystemFacade.Defenition
{
    public enum SizeUnits
    {
        Byte,
        KB,
        MB,
        GB
    }
}