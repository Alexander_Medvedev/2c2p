﻿using System;
using tctp.SystemFacade.Defenition;

namespace tctp.SystemFacade.Extensions
{
    public static class LongExtensions
    {
        public static string ToSize(this long value, SizeUnits unit)
        {
            return $"{value / Math.Pow(1024, (int) unit):0} {unit}";
        }
    }
}