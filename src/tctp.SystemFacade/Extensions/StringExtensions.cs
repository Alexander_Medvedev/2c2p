﻿using System;
using System.Globalization;
using System.Linq;

namespace tctp.SystemFacade.Extensions
{
    public static class StringExtensions
    {
        public static bool CanBeAValidDate(this string value, string[] dateFormats)
        {
            return dateFormats.Any(x => DateTime.TryParseExact(value, dateFormats,
                CultureInfo.CurrentCulture, DateTimeStyles.None, out _));
        }
    }
}