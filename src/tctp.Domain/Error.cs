﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace tctp.Domain
{
    [Table("Errors")]
    public class Error : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }
        public string Message { get; set; }
        public int Record { get; set; }
        public string FileName { get; set; }
        public DateTime Date { get; set; }
    }
}