﻿using System;
using System.ComponentModel.DataAnnotations.Schema;
using tctp.Domain.Defenition;

namespace tctp.Domain
{
    [Table("Transactions")]
    public class Transaction : IEntity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string TransactionId { get; set; }
        public DateTime Date { get; set; }
        public string CurrencyCode { get; set; }
        public decimal Amount { get; set; }
        public Status Status { get; set; }
    }
}