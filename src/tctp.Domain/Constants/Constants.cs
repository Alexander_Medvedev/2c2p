﻿namespace tctp.Domain.Constants
{
    public static class Constants
    {
        public static readonly string[] CsvDateFormats = {"dd/MM/yyyy HH:mm:ss"};
        public static readonly string[] XmlDateFormats = {"yyyy-MM-ddTHH:mm:ss"};
    }
}