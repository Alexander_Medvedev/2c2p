﻿using System;
using System.Globalization;
using CsvHelper;
using CsvHelper.Configuration;
using CsvHelper.TypeConversion;
using tctp.BusinessFacade.Definitions;
using tctp.Domain.Constants;

namespace tctp.BusinessFacade.Model
{
    public class CsvTransactionDto
    {
        [CsvHelper.Configuration.Attributes.Index(0)]
        public string Id { get; set; }

        [CsvHelper.Configuration.Attributes.Index(1)]
        public decimal? Amount { get; set; }

        [CsvHelper.Configuration.Attributes.Index(2)]
        public string CurrencyCode { get; set; }

        [CsvHelper.Configuration.Attributes.Index(3)]
        [CsvHelper.Configuration.Attributes.TypeConverter(typeof(DateTimeCustomConverter))]
        public DateTime? Date { get; set; }

        [CsvHelper.Configuration.Attributes.Index(3)]
        public string DateString { get; set; }

        [CsvHelper.Configuration.Attributes.Index(4)]
        public CsvStatuses? Status { get; set; }

        [CsvHelper.Configuration.Attributes.Index(4)]
        public string StatusString { get; set; }
    }

    public class DateTimeCustomConverter : DefaultTypeConverter
    {
        public override object ConvertFromString(string text, IReaderRow row, MemberMapData memberMapData)
        {
            foreach (var style in Constants.CsvDateFormats)
            {
                if (DateTime.TryParseExact(text, style, CultureInfo.CurrentCulture, DateTimeStyles.None, out var date))
                    return date;
            }

            return null;
        }
    }
}