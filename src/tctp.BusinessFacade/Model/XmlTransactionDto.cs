﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Xml.Serialization;
using tctp.BusinessFacade.Definitions;
using tctp.Domain.Constants;

namespace tctp.BusinessFacade.Model
{
    [XmlRoot(ElementName = "Transactions")]
    public class XmlTransactionCollectionDto
    {
        [XmlElement(ElementName = "Transaction")]
        public List<XmlTransactionDto> Transaction { get; set; }
    }

    [XmlRoot(ElementName = "Transaction")]
    public class XmlTransactionDto
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }

        [XmlElement(ElementName = "PaymentDetails")]
        public PaymentDetails PaymentDetails { get; set; }

        [XmlIgnore]
        public DateTime? TransactionDate { get; set; }

        [XmlElement(ElementName = "TransactionDate")]
        public string TransactionDateString
        {
            get => TransactionDate.HasValue
                ? TransactionDate.Value.ToString(Constants.XmlDateFormats[0])
                : string.Empty;
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    TransactionDate = null;
                }
                else
                {
                    foreach (var style in Constants.XmlDateFormats)
                    {
                        if (DateTime.TryParseExact(value, style, CultureInfo.CurrentCulture, DateTimeStyles.None,
                            out var date))
                            TransactionDate = date;
                    }
                }
            }
        }

        [XmlIgnore]
        public XmlStatuses? Status { get; set; }

        [XmlElement(ElementName = "Status")]
        public string StatusString
        {
            get => Status.ToString();
            set
            {
                if (string.IsNullOrEmpty(value))
                {
                    Status = null;
                }
                else
                {
                    if (Enum.TryParse(value, true, out XmlStatuses theStatus))
                        Status = theStatus;
                    else
                        Status = null;
                }
            }
        }
    }

    [XmlRoot(ElementName = "PaymentDetails")]
    public class PaymentDetails
    {
        [XmlElement(ElementName = "Amount")]
        public decimal? Amount { get; set; }

        [XmlElement(ElementName = "CurrencyCode")]
        public string CurrencyCode { get; set; }
    }
}