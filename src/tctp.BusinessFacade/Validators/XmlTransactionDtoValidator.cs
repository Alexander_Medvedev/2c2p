﻿using FluentValidation;
using tctp.BusinessFacade.Model;
using tctp.Domain.Constants;
using tctp.SystemFacade.Extensions;

namespace tctp.BusinessFacade.Validators
{
    public class XmlTransactionDtoValidator : AbstractValidator<XmlTransactionDto>
    {
        public XmlTransactionDtoValidator()
        {
            RuleFor(x => x.Id).NotEmpty().MaximumLength(50);
            RuleFor(x => x.PaymentDetails).NotEmpty().SetValidator(new PaymentDetailsValidator());
            RuleFor(x => x.TransactionDateString)
                .Must(x => x.CanBeAValidDate(Constants.XmlDateFormats))
                .WithMessage("Invalid date");
            RuleFor(x => x.StatusString).NotEmpty();
        }
    }

    public class PaymentDetailsValidator : AbstractValidator<PaymentDetails>
    {
        public PaymentDetailsValidator()
        {
            RuleFor(x => x.Amount).NotEmpty();
            RuleFor(x => x.CurrencyCode).NotEmpty();
        }
    }
}