﻿using FluentValidation;
using tctp.BusinessFacade.Definitions;
using tctp.BusinessFacade.Model;
using tctp.Domain.Constants;
using tctp.SystemFacade.Extensions;

namespace tctp.BusinessFacade.Validators
{
    public class CsvTransactionDtoValidator : AbstractValidator<CsvTransactionDto>
    {
        public CsvTransactionDtoValidator()
        {
            RuleFor(x => x.Id).NotEmpty().MaximumLength(50);
            RuleFor(x => x.Amount).NotEmpty();
            RuleFor(x => x.CurrencyCode).NotEmpty();
            RuleFor(x => x.DateString)
                .Must(x => x.CanBeAValidDate(Constants.CsvDateFormats))
                .WithMessage("Invalid date");
            RuleFor(x => x.StatusString).IsEnumName(typeof(CsvStatuses));
        }
    }
}