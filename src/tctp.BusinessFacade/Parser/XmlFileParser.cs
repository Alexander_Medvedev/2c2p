﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml;
using System.Xml.Serialization;
using FluentValidation;
using tctp.BusinessFacade.Model;
using tctp.Domain;
using tctp.Domain.Defenition;

namespace tctp.BusinessFacade.Parser
{
    public class XmlFileParser : IFileParser
    {
        private readonly AbstractValidator<XmlTransactionDto> dataValidator;

        public XmlFileParser(AbstractValidator<XmlTransactionDto> dataValidator)
        {
            this.dataValidator = dataValidator;
        }

        public List<Transaction> Parse<T>(Stream file, string fileName, out List<Error> errors)
        {
            return ReadDataFromXml(file, fileName, out errors);
        }

        private List<Transaction> ReadDataFromXml(Stream file, string fileName, out List<Error> errors)
        {
            errors = new List<Error>();
            XmlTransactionCollectionDto data;
            var serializer = new XmlSerializer(typeof(XmlTransactionCollectionDto));
            using (var reader = XmlReader.Create(file))
            {
                try
                {
                    data = (XmlTransactionCollectionDto) serializer.Deserialize(reader);
                }
                catch (Exception e)
                {
                    errors.Add(new Error
                    {
                        Message = $"Critical Error. Msg: {e}",
                        FileName = fileName,
                        Date = DateTime.Now
                    });
                    return new List<Transaction>();
                }
            }

            for (var rowNumber = 0; rowNumber < data.Transaction.Count; rowNumber++)
            {
                var validateResult = dataValidator.Validate(data.Transaction[rowNumber]);
                if (!validateResult.IsValid)
                {
                    errors.Add(new Error
                    {
                        Record = rowNumber,
                        Message = string.Join(";", validateResult.Errors),
                        FileName = fileName,
                        Date = DateTime.Now
                    });
                }
            }

            return !errors.Any()
                ? Map(data.Transaction)
                : new List<Transaction>();
        }

        private static List<Transaction> Map(IEnumerable<XmlTransactionDto> data)
        {
            return data.Select(x => new Transaction
            {
                TransactionId = x.Id,
                Date = x.TransactionDate ?? default,
                CurrencyCode = x.PaymentDetails?.CurrencyCode,
                Amount = x.PaymentDetails?.Amount ?? 0,
                Status = x.Status.HasValue ? (Status) x.Status.Value : Status.A
            }).ToList();
        }
    }
}