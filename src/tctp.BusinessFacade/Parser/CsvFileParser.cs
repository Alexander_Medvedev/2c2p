﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using CsvHelper;
using CsvHelper.Configuration;
using FluentValidation;
using tctp.BusinessFacade.Model;
using tctp.Domain;
using tctp.Domain.Defenition;

namespace tctp.BusinessFacade.Parser
{
    public class CsvFileParser : IFileParser
    {
        private readonly AbstractValidator<CsvTransactionDto> dataValidator;

        public CsvFileParser(AbstractValidator<CsvTransactionDto> dataValidator)
        {
            this.dataValidator = dataValidator;
        }

        public List<Transaction> Parse<T>(Stream file, string fileName, out List<Error> errors)
        {
            return ReadDataFromCsv(file, fileName, out errors);
        }

        private List<Transaction> ReadDataFromCsv(Stream stream, string fileName, out List<Error> errors)
        {
            var data = new List<CsvTransactionDto>();

            using (var reader = new StreamReader(stream))
            {
                using (var csv = new CsvReader(reader, CultureInfo.CurrentCulture))
                {
                    csv.Configuration.HasHeaderRecord = false;
                    csv.Configuration.IgnoreBlankLines = true;
                    csv.Configuration.TrimOptions = TrimOptions.Trim;

                    errors = new List<Error>();
                    for (var rowNumber = 0; csv.Read(); rowNumber++)
                    {
                        try
                        {
                            var record = csv.GetRecord<CsvTransactionDto>();
                            var validateResult = dataValidator.Validate(record);
                            if (validateResult.IsValid)
                            {
                                data.Add(record);
                            }
                            else
                            {
                                errors.Add(new Error
                                {
                                    Record = rowNumber,
                                    Message = string.Join(";", validateResult.Errors),
                                    FileName = fileName,
                                    Date = DateTime.Now
                                });
                            }
                        }
                        catch (Exception e)
                        {
                            errors.Add(new Error
                            {
                                Message = $"Critical Error. Msg: {e}",
                                FileName = fileName,
                                Date = DateTime.Now
                            });
                        }
                    }
                }
            }

            return Map(data);
        }

        private static List<Transaction> Map(IEnumerable<CsvTransactionDto> data)
        {
            return data.Select(x => new Transaction
            {
                TransactionId = x.Id,
                Amount = x.Amount ?? 0,
                CurrencyCode = x.CurrencyCode,
                Date = x.Date ?? default,
                Status = x.Status.HasValue ? (Status) x.Status.Value : Status.A
            }).ToList();
        }
    }
}