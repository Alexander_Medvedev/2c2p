﻿using System.Collections.Generic;
using System.IO;
using tctp.Domain;

namespace tctp.BusinessFacade.Parser
{
    public interface IParserManager<T> where T : class
    {
        List<T> Parse(Stream file, string fileName, out List<Error> errors);
    }
}