﻿using System.Collections.Generic;
using System.IO;
using tctp.Domain;

namespace tctp.BusinessFacade.Parser
{
    public interface IFileParser
    {
        List<Transaction> Parse<T>(Stream file, string fileName, out List<Error> errors);
    }
}