﻿using System;
using System.Collections.Generic;
using System.IO;
using tctp.BusinessFacade.Validators;
using tctp.Domain;

namespace tctp.BusinessFacade.Parser
{
    public class ParserManager : IParserManager<Transaction>
    {
        public List<Transaction> Parse(Stream file, string fileName, out List<Error> errors)
        {
            return GetParser(fileName.Substring(fileName.LastIndexOf('.')))
                .Parse<Transaction>(file, fileName, out errors);
        }

        private static IFileParser GetParser(string fileName)
        {
            switch (fileName)
            {
                case ".xml":
                    return new XmlFileParser(new XmlTransactionDtoValidator());
                case ".csv":
                    return new CsvFileParser(new CsvTransactionDtoValidator());
                default:
                    throw new Exception("Unsupported file type");
            }
        }
    }
}