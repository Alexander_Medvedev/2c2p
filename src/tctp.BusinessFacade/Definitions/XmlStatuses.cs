﻿namespace tctp.BusinessFacade.Definitions
{
    public enum XmlStatuses
    {
        Approved,
        Rejected,
        Done
    }
}