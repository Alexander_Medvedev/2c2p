﻿namespace tctp.BusinessFacade.Definitions
{
    public enum CsvStatuses
    {
        Approved,
        Failed,
        Finished
    }
}