﻿using System.Collections.Generic;
using tctp.Domain;

namespace tctp.DataService
{
    public interface IRepository<T> where T : IEntity
    {
        void Add(T entity);
        void Add(IEnumerable<T> entity);
        List<T> GetAll();
    }
}