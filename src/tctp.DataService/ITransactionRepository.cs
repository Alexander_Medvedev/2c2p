﻿using System;
using System.Collections.Generic;
using tctp.Domain;
using tctp.Domain.Defenition;

namespace tctp.DataService
{
    public interface ITransactionRepository : IRepository<Transaction>
    {
        List<Transaction> GetByCurrency(string code);
        List<Transaction> GetByDateRange(DateTime from, DateTime to);
        List<Transaction> GetByStatus(Status status);
    }
}