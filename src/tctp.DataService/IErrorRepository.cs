﻿using System.Collections.Generic;
using System.Linq;
using tctp.Domain;

namespace tctp.DataService
{
    public class ErrorRepository : IRepository<Error>
    {
        public void Add(Error error)
        {
            using (var ctx = new DbContext())
            {
                ctx.Errors.Add(error);
                ctx.SaveChanges();
            }
        }

        public void Add(IEnumerable<Error> errors)
        {
            using (var ctx = new DbContext())
            {
                ctx.Errors.AddRange(errors);
                ctx.SaveChanges();
            }
        }

        public List<Error> GetAll()
        {
            using (var ctx = new DbContext())
            {
                return ctx.Errors.ToList();
            }
        }
    }
}