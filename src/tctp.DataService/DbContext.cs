﻿using System.Data.Entity;
using tctp.Domain;

namespace tctp.DataService
{
    public class DbContext : System.Data.Entity.DbContext
    {
        public DbContext()
            : base("DbConnection")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<DbContext>());
        }

        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Error> Errors { get; set; }
    }
}
