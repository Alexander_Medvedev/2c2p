﻿using System;
using System.Collections.Generic;
using System.Linq;
using tctp.Domain;
using tctp.Domain.Defenition;

namespace tctp.DataService
{
    public class TransactionRepository : ITransactionRepository
    {
        public void Add(Transaction transaction)
        {
            using (var ctx = new DbContext())
            {
                ctx.Transactions.Add(transaction);
                ctx.SaveChanges();
            }
        }

        public void Add(IEnumerable<Transaction> transactions)
        {
            using (var ctx = new DbContext())
            {
                ctx.Transactions.AddRange(transactions);
                ctx.SaveChanges();
            }
        }

        public List<Transaction> GetAll()
        {
            using (var ctx = new DbContext())
            {
                return ctx.Transactions.ToList();
            }
        }

        public List<Transaction> GetByCurrency(string code)
        {
            using (var ctx = new DbContext())
            {
                return ctx.Transactions
                    .Where(x => x.CurrencyCode.Equals(code, StringComparison.InvariantCultureIgnoreCase)).ToList();
            }
        }

        public List<Transaction> GetByDateRange(DateTime from, DateTime to)
        {
            using (var ctx = new DbContext())
            {
                to = to.AddDays(1);
                return ctx.Transactions.Where(x => x.Date >= from && x.Date < to).ToList();
            }
        }

        public List<Transaction> GetByStatus(Status status)
        {
            using (var ctx = new DbContext())
            {
                return ctx.Transactions.Where(x => x.Status == status).ToList();
            }
        }
    }
}